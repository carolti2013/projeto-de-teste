package com.prova.api.Restricoes;


import static io.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.junit.Test;

import com.prova.BaseTest;
import com.prova.domain.Restricoes;

public class RestricoesTest extends BaseTest{

    private static final String SHOW_RESTRICOES_ENDPOINT = "/restricoes/{cpf}";
    @Test
    public void cpfSemRestricao(){
        String uri = getUri(SHOW_RESTRICOES_ENDPOINT);

        Restricoes restricoes = given().
            log().all().
            pathParam("cpf", "59259182077").
        when().
        log().all().
            get(uri).
        then().
        log().all().
            contentType(ContentType.JSON).
            statusCode(HttpStatus.SC_OK).
        extract().
            body().jsonPath().getObject("", Restricoes.class);

        assertThat(restricoes.getMensagem(), containsString("Não possui restrição"));
        
    }
    @Test
    public void cpfComRestricao(){
        String uri = getUri(SHOW_RESTRICOES_ENDPOINT);

        Restricoes restricoes = given().
            pathParam("cpf", "97093236014").
        when().
            get(uri).
        then().
            contentType(ContentType.JSON).
            statusCode(HttpStatus.SC_NO_CONTENT).
        extract().
            body().jsonPath().getObject("", Restricoes.class);


        
        assertThat(restricoes.getMensagem(), containsString("O CPF 97093236014 possui restrição"));
        
    }

   
    
    
}
