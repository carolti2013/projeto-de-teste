package com.prova.api.Simulacoes;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import com.prova.BaseTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpStatus;
import org.junit.Test;




public class SimulacaoTest extends BaseTest {

    private static final String LIST_SIMULACAO_ENDPOINT = "/simulacoes";
    private static final String SHOW_SIMULACAO_ENDPOINT = "/simulacoes/{cpf}";
    private static final String CREATE_SIMULACAO_ENDPOINT = "/simulacoes";
    private static final String UPDATE_SIMULACAO_ENDPOINT = "/simulacoes/{cpf}";
    private static final String DELETE_SIMULACAO_ENDPOINT = "/simulacoes/{id}";
    private static final String DELETE_SIMULACAO_CPF_ENDPOINT = "/simulacoes/{cpf}";

    @Test
    public void listarTodasSimulacoes() {
        String uri = getUri(LIST_SIMULACAO_ENDPOINT);
         given()
                .when()
                .log().all()
                .get(uri)
                .then()
                .log().all().contentType(ContentType.JSON).statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void mostrarSimulacaoPorCpf() {

        String uri = getUri(SHOW_SIMULACAO_ENDPOINT);

      Response response = given()
      .log().all()
      .pathParam("cpf", "07737468030")
      .when()
      .log().all()
      .get(uri)
      .then()
                .log().all()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.SC_OK);

                String cpf = response.jsonPath().getString("cpf");
        
    }

    @Test
    public void criarUmaSimulacao() {
        String uri = getUri(CREATE_SIMULACAO_ENDPOINT);

        Map<String, String> simulacoes = new HashMap<>();
        simulacoes.put("nome", "Carolina");
        simulacoes.put("cpf", "58482628020");
        simulacoes.put("email", "teste@teste.com");
        simulacoes.put("valor", "1500");
        simulacoes.put("parcelas", "5");
        simulacoes.put("seguro", "true");

         given()
                .contentType(ContentType.JSON)
                .body(simulacoes)
                .when()
                .log().all()
                .post(uri)
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_CREATED)
                .body("nome", is("Carolina"))
                .body("cpf", is("58482628020"));

    }

    @Test
    public void verificarCpf() {
        String uri = getUri(CREATE_SIMULACAO_ENDPOINT);

        Map<String, String> simulacoes = new HashMap<>();
        simulacoes.put("nome", "Carolina");
        simulacoes.put("cpf", "79008004097");
        simulacoes.put("email", "teste@teste.com");
        simulacoes.put("valor", "1500");
        simulacoes.put("parcelas", "5");
        simulacoes.put("seguro", "true");

        given()
                .body(simulacoes)
                .when()
                .log().all()
                .post(uri)
                .then()
                .log().all()
                .statusCode(400)
                .body("mensagem", is("CPF duplicado"));

    }

    @Test
    public void verificarErro() {

        String uri = getUri(CREATE_SIMULACAO_ENDPOINT);

        Map<String, String> simulacoes = new HashMap<>();
        simulacoes.put("nome", "Carolina Teste");
        simulacoes.put("cpf", "0773746803");
        simulacoes.put("email", "email.email.com");
        simulacoes.put("valor", "41000");
        simulacoes.put("parcelas", "0");
        simulacoes.put("seguro", "true");

        given()
                .body(simulacoes)
                .when()
                .log().all()
                .post(CREATE_SIMULACAO_ENDPOINT)
                .then()
                .log().all()
                .statusCode(400)
                .body("erros", is(notNullValue()))
                .body("erros.parcelas", is("Parcelas deve ser igual ou maior que 2"))
                .body("erros.valor", is("Valor deve ser menor ou igual a R$ 40.000"))
                .body("erros.email", is("E-mail deve ser um e-mail válido"));

    }

    @Test
    public void atualizarSimulacao() {
        String uri = getUri(UPDATE_SIMULACAO_ENDPOINT);

        Map<String, String> simulacoes = new HashMap<>();
        simulacoes.put("nome", "Carolina Test Api");
        simulacoes.put("cpf", "58482628020");
        simulacoes.put("email", "email@teste.br");
        simulacoes.put("valor", "1500");
        simulacoes.put("parcelas", "10");
        simulacoes.put("seguro", "true");

        given()
                .pathParam("cpf", "58482628020")
                .body(simulacoes)
                .when()
                .log().all()
                .put(uri)
                .then()
                .log().all()
                .statusCode(200)
                .body("nome", is("Carolina Test Api"))
                .body("email", is("email@teste.br"));

    }

    @Test
    public void atualizarSimulacaoComCpfNaoCadastrado() {
        String uri = getUri(UPDATE_SIMULACAO_ENDPOINT);

        Map<String, String> simulacoes = new HashMap<>();
        simulacoes.put("nome", "Carolina Test Api");
        simulacoes.put("cpf", "78122526047");
        simulacoes.put("email", "email@teste.br");
        simulacoes.put("valor", "1500");
        simulacoes.put("parcelas", "10");
        simulacoes.put("seguro", "true");

        given()
                .pathParam("cpf", "78122526047")
                .body(simulacoes)
                .when()
                .log().all()
                .put(uri)
                .then()
                .log().all()
                .statusCode(404);

    }

    @Test
    public void deletarSimulacaoId() {
        String uri = getUri(DELETE_SIMULACAO_CPF_ENDPOINT);

        given()
                .pathParam("id", "13")
                .when()
                .log().all()
                .delete(uri)
                .then()
                .log().all()
                .statusCode(200);

    }

    @Test
    public void deletarSimulacaoCpf() {
        String uri = getUri(DELETE_SIMULACAO_CPF_ENDPOINT);

        given()
                .pathParam("cpf", "95398641000")
                .when()
                .log().all()
                .delete(DELETE_SIMULACAO_CPF_ENDPOINT)
                .then()
                .log().all()
                .statusCode(200);

    }

    @Test
    public void simulacaoNaoEncontrada() {
        String uri = getUri(SHOW_SIMULACAO_ENDPOINT);
        
        given()
                .pathParam("cpf", "95398641000")
                .when()
                .log().all()
                .get(uri)
                .then()
                .log().all()
                .statusCode(404)
                .body("mensagem", is("CPF 95398641000 não encontrado"));

    }
}
