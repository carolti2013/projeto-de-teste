package com.prova;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import org.junit.BeforeClass;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;

public class BaseTest {

    private static final String BASE_URL = "http://localhost:8080";
    private static final String BASE_PATH = "/api/v1";

    

 

    @BeforeClass
    public static void setup() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    protected String getUri(String endpoint) {
        return BASE_URL + BASE_PATH + endpoint;
    }
}
